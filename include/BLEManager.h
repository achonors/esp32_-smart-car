#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include"byte_data.h"



extern byte_data BLEByteData;

class BLEManager
{
private:
    static BLECharacteristic* pSendCharacteristic;

public:
    static bool mIsConnected;


    static void initBLE(String bleName);
    static void sendData(uint8_t* bytes, short length);
    static bool isConnected();
};