#include "esp_camera.h"
#include <WiFi.h>

#define CAMERA_MODEL_AI_THINKER
#include "camera_pins.h"


class CameraWebServer{

    public:
        static void initCamera();
        static void setFramesize(framesize_t size);
};