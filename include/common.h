
typedef enum {
    CHANGE_BLE_NAME = 0,    //修改蓝牙名
    CHANGE_WIFI_CONFIG,     //修改WIFI配置
    CHANGE_FRAMESIZE,       //修改相机分辨率

    SET_LED,                //修改闪光灯状态
    SET_DIRECTION,          //修改小车方向
} protocol_type;