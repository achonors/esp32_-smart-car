#include <stdint.h>

//对多保存256字节
#define BUFFER_LEN (256)

//回调函数
typedef void (DataCallBack)(const uint8_t* buffer, short length);

class byte_data{
    private:
        short start = 0;
        short end = 0;
        uint8_t mBytes[BUFFER_LEN];
        DataCallBack* mCallback = nullptr;
        bool checkBuffer();
    public:
        void reset();
        int getDataLength();
        void writeBytes(uint8_t* bytes, short length);
        void registerCallback(DataCallBack* callback);
};