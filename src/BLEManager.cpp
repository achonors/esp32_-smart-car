#include <Arduino.h>
#include "BLEManager.h"

const char* BLE_UUID_SERVICE  = "6E400001-B5A3-F383-E1A9-E50E24DCCA9E";
const char* BLE_UUID_RX       = "6E400002-B5A3-F383-E1A9-E50E24DCCA9E";
const char* BLE_UUID_TX       = "6E400003-B5A3-F383-E1A9-E50E24DCCA9E";

byte_data BLEByteData;

bool BLEManager::mIsConnected = false;
BLECharacteristic* BLEManager::pSendCharacteristic = nullptr;

class MBLEServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      BLEManager::mIsConnected = true;
    };
    void onDisconnect(BLEServer* pServer) {
      BLEManager::mIsConnected = false;
    }
};

//特性回调
class MBLECharacteristicCallbacks : public BLECharacteristicCallbacks
{
    void onWrite(BLECharacteristic *pCharacteristic)
    {
        std::string rxValue = pCharacteristic->getValue();

        if (0 < rxValue.length())
        {
            uint8_t data[rxValue.length()];
            for (int i = 0; i < rxValue.length(); i++){
                data[i] = (int)rxValue[i];
                //打印
                //Serial.print((int)rxValue[i]);
                //Serial.print("|");
            }
            //Serial.println("");
            // 写入缓存
            BLEByteData.writeBytes(data, rxValue.length());
        }
    }
};

void BLEManager::initBLE(String bleName){
    //初始化一个蓝牙设备
    BLEDevice::init(bleName.c_str());

    // 创建一个蓝牙服务器
    BLEServer *pServer = BLEDevice::createServer();
    
    //服务器回调函数设置为MyServerCallbacks
    pServer->setCallbacks(new MBLEServerCallbacks());

    //创建一个BLE服务
    BLEService *pService = pServer->createService(BLE_UUID_SERVICE);

    pSendCharacteristic = pService->createCharacteristic(BLE_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);
    //创建一个(读)特征值 类型是通知
    pSendCharacteristic->addDescriptor(new BLE2902());
    //为特征添加一个描述

    BLECharacteristic* pCharacteristic = pService->createCharacteristic(BLE_UUID_RX, BLECharacteristic::PROPERTY_WRITE);
    //创建一个(写)特征 类型是写入
    pCharacteristic->setCallbacks(new MBLECharacteristicCallbacks());
    //为特征添加一个回调

    //开启服务
    pService->start();
    //服务器开始广播
    pServer->getAdvertising()->start();
}

void BLEManager::sendData(uint8_t* bytes, short length){
    pSendCharacteristic->setValue(bytes, length);
    pSendCharacteristic->notify();
}

bool BLEManager::isConnected(){
    return mIsConnected;
}