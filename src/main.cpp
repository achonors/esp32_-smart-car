#include <Arduino.h>
#include <Preferences.h>
#include <WiFi.h>

#include "common.h"
#include "BLEManager.h"
#include "CameraWebServer.h"

WiFiServer server;
Preferences preferences;

//TCPServer收到的数据
byte_data serverData;

//PREF的Key
const char *PREF_SSID = "ssid";
const char *PREF_PASSWORD = "password";
const char *PREF_BLE_NAME = "ble_name";

void connectWIFI()
{
	//获取WIFI账号密码
	preferences.begin("WIFI");
	String ssid = preferences.getString(PREF_SSID, "achonorWIFI");
	String password = preferences.getString(PREF_PASSWORD, "18075952730");
	preferences.end();
	
	WiFi.mode(WIFI_STA);
	WiFi.setSleep(false);
	//连接
	WiFi.begin(ssid.c_str(), password.c_str());

	Serial.print("SSID: ");
	Serial.println(ssid);
	Serial.print("Password: ");
	Serial.println(password);

	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("WiFi连接成功 ：");
	Serial.println(WiFi.localIP());
}

void changeWIFI(String ssid, String password)
{
	//保存
	preferences.begin("WIFI");
	preferences.putString(PREF_SSID, ssid);
	preferences.putString(PREF_PASSWORD, password);
	preferences.end();

	//重连
	connectWIFI();
}

void initBLE()
{
	preferences.begin("BLE");
	String bleName = preferences.getString(PREF_BLE_NAME, "achonorCar");
	preferences.end();

	BLEManager::initBLE(bleName);
		
	Serial.print("BLEName: ");
	Serial.println(bleName);
	Serial.println("初始化蓝牙完成");
}

void changeBLEName(String bleName)
{
	//保存
	preferences.begin("BLE");
	preferences.putString(PREF_BLE_NAME, bleName);
	preferences.end();
}

int16_t calcDutyCycle(uint8_t u_value){
	int16_t value = ((int16_t)u_value) - 100;
	if (0 == value){
		return 0;
	}
	if (0 < value){
		return 723 + (3 * value);
	}else{
		return -723 + (3 * value);
	}
}

void setDirection(uint8_t right, uint8_t left){
	int16_t r_value = calcDutyCycle(right);
	if (0 == r_value){
		ledcWrite(8, 0);
		ledcWrite(9, 0);
	}else if (0 < r_value){
		ledcWrite(8, r_value);
		ledcWrite(9, 0);
	}else{
		ledcWrite(8, 0);
		ledcWrite(9, -r_value);
	}

	int16_t l_value = calcDutyCycle(left);
	if (0 == l_value){
		ledcWrite(10, 0);
		ledcWrite(11, 0);
	}else if (0 < l_value){
		ledcWrite(10, l_value);
		ledcWrite(11, 0);
	}else{
		ledcWrite(10, 0);
		ledcWrite(11, -l_value);
	}
}

void handleProtocol(const uint8_t* proto, short length){
	if (length <= 2 || 0x43 != proto[0]){
		return;
	}
	protocol_type p_type = (protocol_type)proto[2];
	if (CHANGE_BLE_NAME == p_type){
		//修改蓝牙名字
	}else if (CHANGE_WIFI_CONFIG == p_type){
		//修改WIFI配置
	}else if (CHANGE_FRAMESIZE == p_type){
		//修改相机分辨率
		CameraWebServer::setFramesize((framesize_t)proto[3]);
	}else if (SET_LED == p_type){
		//设置闪光灯
		digitalWrite(4, proto[3]);
	}else if (SET_DIRECTION == p_type){
		//设置方向
		setDirection(proto[3], proto[4]);
	}
}

//收到数据的回调
void onReceivedData(const uint8_t* buffer, short length){
	for(int i = 0; i < length; i++){
		Serial.print(buffer[i]);
		Serial.print("|");
	}
	Serial.println("");
	//处理协议
	handleProtocol(buffer, length);
}


void startServer(void *pvParameters){
	//连接WIFI
	connectWIFI();
	//初始化摄像头
	CameraWebServer::initCamera();
	//开启TCP服务,监听4567端口
	server.begin(4567);
	server.setNoDelay(true);
}

void setup()
{
	// put your setup code here, to run once:
	Serial.begin(115200);
	Serial.setDebugOutput(true);
	Serial.println();
	//闪光灯IO
	pinMode(4, OUTPUT);
	digitalWrite(4, 0);
	//配置GPIO
	pinMode(2, INPUT_PULLDOWN);

	if (digitalRead(2)){
		Serial.println("配置模式启动");
		//蓝牙配置模式
		initBLE();
		//注册蓝牙回调
		BLEByteData.registerCallback(onReceivedData);
	}else{
		Serial.println("普通模式启动");
		startServer(nullptr);
		//注册TCP回调
		serverData.registerCallback(onReceivedData);

		//初始化ledc
		ledcSetup(8, 1000, 10);
		ledcSetup(9, 1000, 10);
		ledcSetup(10, 1000, 10);
		ledcSetup(11, 1000, 10);

		//ledc指向GPIO
		ledcAttachPin(14, 8);
		ledcAttachPin(15, 9);
		ledcAttachPin(12, 10);
		ledcAttachPin(13, 11);
		//设置默认占空比
		ledcWrite(8, 0);
		ledcWrite(9, 0);
		ledcWrite(10, 0);
		ledcWrite(11, 0);
	}
}


void loop()
{
	//尝试建立客户对象
	WiFiClient client = server.available();
    if (client){
        uint8_t readBuffer[20];
        Serial.print("客户端连接成功");
		Serial.println(client.remoteIP());
		//重置数据队列
		serverData.reset();
		client.setNoDelay(true);
		
		// delay(5000);
		// Serial.println("发送测试数据");
		// uint8_t data[4] = {0x43, 0x04, 0x00, 0x00};
		// client.write(data, 4);
        while (client.connected()){
			//连接中
            if (client.available()){
				//读取数据
                readBuffer[0] = client.read();
				serverData.writeBytes(readBuffer, 1);
            }
			delay(10);
        }
		//结束当前连接:
        client.stop();
        Serial.println("客户端断开连接");
    }
}

