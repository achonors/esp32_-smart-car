#include "byte_data.h"


void byte_data::reset(){
    start = 0;
    end = 0;
}

int byte_data::getDataLength(){
    return ((end + BUFFER_LEN) - start) % BUFFER_LEN;
}

void byte_data::writeBytes(uint8_t* bytes, short length){
    for (int i = 0; i < length; i++) {
        mBytes[(i + end) % BUFFER_LEN] = bytes[i];
    }
    end = (end + length) % BUFFER_LEN;
    //检测是否有完整的协议
    while(checkBuffer());
}

bool byte_data::checkBuffer(){
    while (start != end && 0x43 != mBytes[start]) {
        //脏数据
        start = (start + 1) % BUFFER_LEN;
    }
    if (getDataLength() < 2) {
        return false;
    }

    int len = (int)mBytes[(start + 1) % BUFFER_LEN];
    if (getDataLength() < len) {
        return false;
    }
    uint8_t protocolByte[len];
    for (int i = 0; i < len; i++) {
        protocolByte[i] = mBytes[(start + i) % BUFFER_LEN];
    }
    start = (start + len) % BUFFER_LEN;
    //回调
    if (nullptr != mCallback){
        mCallback(protocolByte, len);
    }
    return true;
}

void byte_data::registerCallback(DataCallBack *callback){
    mCallback = callback;
}